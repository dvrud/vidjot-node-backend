// when u install a module and u want to use just use the keyword require to get it. just as below.

const express = require("express");
// getting the handlebars module to use.handlebars areused to design the views or say creating the views.

const exphbs = require("express-handlebars");

//  getting the body parser which we will use to get the values from the form.
const bodyParser = require("body-parser");

//  Creating a path module . It is the built in node module so we dont have to install it again.
const path = require("path");

//  creating a mongoose module to use .just as the others
const mongoose = require("mongoose");
//  Initializing the method override package to change the post req to put req.
const methodOverride = require("method-override");

// initializing the connect flash package
const flash = require("connect-flash");

// Initializing the express session package.
const session = require("express-session");
//To initialize the application
const app = express();

// Bringing in the ideas route file
const ideas = require("./routes/ideas");
// Bringing in the passport
const passport = require("passport");
// Bringing in the users route file.
const users = require("./routes/users");
// Map global promis to get rid of warning in cmd.
// mongoose.Promise = global.Promise;

//bringing in the Passport config
require("./config/passport")(passport);

// Bringing in the database config file
const db = require("./config/database");
// Connecting to mongoose
mongoose
  .connect(db.mongoURI, { useNewUrlParser: true }) //after vidjot dev put a comma and write {useMongoClient : true} but it is not required in version 5 and above so i have removed it.
  .then(() => console.log("Mongo Database Connected ..... !."))
  .catch(err => console.log(err));

// Handlebars midddleware
app.engine(
  "handlebars",
  exphbs({
    defaultLayout: "main"
  })
);
app.set("view engine", "handlebars");

// Creating the body parser middleware.
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// Static folder
app.use(express.static(path.join(__dirname, "public")));
// middleware for the method override package.
app.use(methodOverride("_method"));

// express session middleware
app.use(
  session({
    secret: "my life secret",

    resave: false,
    saveUninitialized: false
  })
);

// Bringing in the passport session and it is very important to pput it after express session middleware or else there will be a big issue

app.use(passport.initialize());
app.use(passport.session());

// Connect flash middleware
app.use(flash());

// Global variables for messages
app.use((req, res, next) => {
  res.locals.success_msg = req.flash("success_msg");
  res.locals.error_msg = req.flash("error_msg");
  res.locals.error = req.flash("error");
  res.locals.user = req.user || null;
  next();
});

// Creating a middleware which takes 3 parameters which is req res and next(to call the next middleware).and we can access req and res object of this middleware anywhere through the app.and i am commenting it as it is not part of app.just for understanding purposes.
// app.use((req , res , next) => {
//   req.name = 'Dharmik Soni';
//   next();
// }) ;

// Creating route for index.route is necessary to display something on the browser.by creating route we can display the content on the browser.Http get req displays the content.
app.get("/", (req, res) => {
  // send method sends the content to the browser that is to be displayed.
  // send method is replaced with render as to display the index handle bars which are the views..(and main handlebar file is the master page which is common in all the view in layout folder.)
  const title = "Welcome";
  res.render("index", {
    title: title
  });
  // console.log(req.name);
});

// creating another route which is for about.
app.get("/about", (req, res) => {
  res.render("about");
});

// Using the routes . here the 1st parameter is to tell that anything that starts with /ideas go in to ideas file which is defined as the second parameter and look up there for that route.
app.use("/ideas", ideas);

// Getting to use the user route.
app.use("/users", users);
// Creating the port.
// process.env.port is necessary to deploy this to heroku cz heroku may use different port or other than 5000 so its necessary.
const port = process.env.PORT || 5000;

// The below method will listen to a certain port.in this case the port variable.
app.listen(port, () => {
  console.log(`Server started on ${port} !`);
});
