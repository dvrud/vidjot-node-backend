const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// Creating idea schema.
const UserSchema = new Schema({
  name:{
    type: String,
    required: true
  },
  email:{
    type: String,
    required: true
  },
  password:{
    type: String,
    required: true
  }
});

// Creating a mongoose model and it takes 2 parameters first is the name of the model and second is the schema of idea which we created above.
mongoose.model('users' , UserSchema);