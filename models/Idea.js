const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// Creating idea schema.
const IdeaSchema = new Schema({
  title:{
    type: String,
    required: true
  },
  details:{
    type: String,
    required: true
  },
  user:{
    type:String,
    required:true
  },
  date:{
    type: Date,
    default: Date.now
  }
});

// Creating a mongoose model and it takes 2 parameters first is the name of the model and second is the schema of idea which we created above.
mongoose.model('ideas' , IdeaSchema);