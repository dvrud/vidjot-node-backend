const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');


// Loading the user models which is database
const user = mongoose.model('users');

module.exports = function(passport){
  passport.use(new LocalStrategy({usernameField: 'email'} , (email,password,done) => {
    // Matching username from the database
    user.findOne({
      email : email
    })
    .then(user => {
      if(!user){
        return done(null , false , {message:'No User found'});
      }
      
      // Matching  password form hte database
      // Here bcrypts first parameter password is the non hashed password which is entered by user and the second parameter is the hashed password for the specific user and then it is comparing both the password.
      bcrypt.compare(password , user.password ,
        (err, isMatch) => {
          if(err) 
          {
            throw (err);
          }
          if(isMatch){
            return done(null , user);
          }
          else{
            return done(null , false , {message: 'Password incorrect'});
          }
      })
    })
  }));

  // Creating session by serializing and deserializing users
  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });
  
  passport.deserializeUser(function(id, done) {
    user.findById(id, function(err, user) {
      done(err, user);
    });
  });
}