// Bringing in the express for the routes
const express = require("express");
// Bringing in the router for accessing the routes here in this file.
const router = express.Router();
// Bringing in the mongoose to use
const mongoose = require("mongoose");
// Bringing in the bcrypt to hash the password for the safe passwords.
const bcrypt = require("bcryptjs");
// Bringing in the passport for the authentication.
const passport = require("passport");

// Loading the user model.
require("../models/User");
const User = mongoose.model("users");
// Creating the login route.
router.get("/login", (req, res) => {
  res.render("users/login");
});

// Creating the register route
router.get("/register", (req, res) => {
  res.render("users/register");
});
// Login form post
router.post("/login", (req, res, next) => {
  passport.authenticate("local", {
    successRedirect: "/ideas",
    failureRedirect: "/users/login",
    failureFlash: true
  })(req, res, next);
});
// Register from post
router.post("/register", (req, res) => {
  let errors = [];

  if (req.body.password != req.body.confirmpassword) {
    errors.push({
      text: "Passwords do not match"
    });
  }

  if (req.body.password.length < 7) {
    errors.push({
      text: "Password must be atleast of 7 characters"
    });
  }

  if (errors.length > 0) {
    res.render("users/register", {
      errors: errors,
      name: req.body.name,
      email: req.body.email
    });
    //  The reason we are passing the values in the render is if there is any error then also the values wont get cleared out of the form and stays in thr form so the user wont have reenter all the values except the password.
  } else {
    User.findOne({ email: req.body.email }).then(user => {
      if (user) {
        req.flash("error_msg", "The email is registered with us");
        res.redirect("/users/register");
      } else {
        const newUser = new User({
          name: req.body.name,
          email: req.body.email,
          password: req.body.password
        });
        //  It will generate gensalt for us menas it will encrypt the password.
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            // The hash passed above is the actual hashed password.
            if (err) throw err;
            newUser.password = hash;
            newUser
              .save()
              .then(user => {
                req.flash(
                  "success_msg",
                  "You are now registered and can login"
                );
                res.redirect("/users/login");
              })
              .catch(err => {
                console.log(err);
                return;
              });
          });
        });
      }
    });
  }
});

// Logout route
router.get("/logout", (req, res) => {
  // req.logout();
  // req.session.destroy();
  // res.redirect("/");
  // req.flash('success_msg' , 'You are successfully logged out');
  req.session.destroy(function() {
    res.clearCookie("connect.sid");
    res.redirect("/");
  });
});
module.exports = router;
