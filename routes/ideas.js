// Bringing in the express for the routes
const express = require("express");
// Bringing in the router for accessing the routes here in this file.
const router = express.Router();
// Bringing in the mongoose to use
const mongoose = require("mongoose");
// Loading authentication helpers
// using the curly braces below we can take any function that we want from any file which in this case is taken from auth file which will help us to authenticated the user and check if the user has the authority to use the req page or not.
const { ensureAuthenticated } = require("../helpers/auth");

// Connecting the ideas model in here.
require("../models/Idea");
const Idea = mongoose.model("ideas");
// From here on there are routes.

// Here we dont have access to app.js so we gonna use router instead which will look like router.get / router.post

// Creating route for displaying the ideas.
// important after.get the one in the ''s is the name of the route which we have to type in the browser to access that page and the one that is in the render is the actual path which is to be given to access that.remember this.
router.get("/", ensureAuthenticated, (req, res) => {
  res.header(
    "Cache-Control",
    "no-cache, private, no-store, must-revalidate,max-stale=0, post-check=0, pre-check=0"
  );
  // finding the data from the mongoose database through the below syntax.
  Idea.find({ user: req.user.id })
    // sorting the data datewise through the below syntax.
    .sort({ date: "desc" })
    // the above find method will return the promise which we are fetching through .then.
    .then(ideas => {
      res.render("ideas/index", {
        ideas: ideas
      });
    });
});
// Creating the route for adding ideas.
router.get("/add", ensureAuthenticated, (req, res) => {
  res.render("ideas/add");
});

// creating the route for editing the ideas.
router.get("/edit/:id", ensureAuthenticated, (req, res) => {
  // the findone method is used to only find the single entry from the database which in this case is the entry with the id whih is fetched by us.
  Idea.findOne({
    _id: req.params.id
  }).then(idea => {
    if (idea.user != req.user.id) {
      req.flash("error_msg", "You are not authorized to edit this idea");
      res.redirect("/ideas");
    } else {
      res.render("ideas/edit", {
        idea: idea
      });
    }
  });
});
// Processing the form.The input from the form uses post request to save the data in database.
// Also to update the database put req is used.which will be done below to update the ideas form.
router.post("/", ensureAuthenticated, (req, res) => {
  let errors = [];

  if (!req.body.title) {
    errors.push({ text: "Please enter the title" });
  }
  if (!req.body.details) {
    errors.push({ text: "Please enter the details" });
  }
  if (errors.length > 0) {
    res.render("ideas/add", {
      errors: errors,
      title: req.body.title,
      details: req.body.details
    });
  } else {
    const newUser = {
      title: req.body.title,
      details: req.body.details,
      user: req.user.id
    };

    new Idea(newUser).save().then(idea => {
      req.flash("success_msg", "Video idea succesfully added");
      res.redirect("/ideas");
    });
  }
});

// Creating the route to update the form.
router.put("/:id", ensureAuthenticated, (req, res) => {
  Idea.findOne({
    _id: req.params.id
  }).then(idea => {
    // new values
    idea.title = req.body.title;
    idea.details = req.body.details;

    idea.save().then(idea => {
      req.flash("success_msg", "Video idea succesfully updated");
      res.redirect("/ideas");
    });
  });
});

// Processing the delete request.
router.delete("/:id", ensureAuthenticated, (req, res) => {
  Idea.deleteOne({ _id: req.params.id }).then(() => {
    req.flash("success_msg", "Video idea succesfully removed");
    res.redirect("/ideas");
  });
});

// this will help exporting the routes here.
module.exports = router;
